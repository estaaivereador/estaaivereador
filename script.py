# -*- coding: utf-8 -*-
import BeautifulSoup
import urllib
import os


def download(url, filename):
	webFile = urllib.urlopen(url)
	localFile = open(filename, 'w')
	localFile.write(webFile.read())
	webFile.close()
	localFile.close()

def removeZero(data):
    d = data.split("/")
    nova_data = d[0].lstrip("0") + "/" + d[1].lstrip("0") + "/" + d[2]
    return nova_data

def colocaZero(data):
    d = data.split("/")
    if len(d[0]) == 1:
        d[0] = "0" + d[0]
    if len(d[1]) == 1:
        d[1] = "0" + d[1]
    return d[0] + "/" + d[1] + "/" + d[2]

def f5(seq, idfun=None):  
    # order preserving 
    if idfun is None: 
        def idfun(x): return x 
    seen = {} 
    result = [] 
    for item in seq: 
        marker = idfun(item) 
        # in old Python versions: 
        # if seen.has_key(marker) 
        # but in new ones: 
        if marker in seen: continue 
        seen[marker] = 1 
        result.append(item) 
    return result

def loadSessoes():
    xml = urllib.urlopen('Votacoes_2011.xml').read()
    soup = BeautifulSoup.BeautifulStoneSoup(xml)
    sessoes = soup.findAll("votacao")
    lista_sessoes = []   
    for sessao in sessoes:
        lista_sessoes.append(sessao['datadasessao'])
    lista_sessoes = f5(lista_sessoes)
    return lista_sessoes

def loadVotacao():
    xml = urllib.urlopen('Votacoes_2011.xml').read()
    votacoes = BeautifulSoup.BeautifulStoneSoup(xml)
    return votacoes

def loadVotos(data, votacoes):
    vereadores = votacoes.findAll("vereador", { "datadasessao" : data })
    return vereadores

def loadPresenca(data):
    data = colocaZero(data)
    d = data.split("/")

    base_url = "http://www2.camara.sp.gov.br/SIP/BaixarXML.aspx?arquivo="
    arquivo = "Presencas_" + d[2] + "_" + d[1] + "_" + d[0] + ".xml"
    if not os.path.isfile(arquivo):
        print 'Downloading ' + arquivo
        download(base_url + arquivo,arquivo)

    xml = urllib.urlopen(arquivo).read()
    soup = BeautifulSoup.BeautifulStoneSoup(xml)
    vereadores = soup.findAll("vereador")
    return vereadores

def checaPresenca(data, votacoes):
    lista_votos = loadVotos(data, votacoes)
    lista_presenca = loadPresenca(data)
    for vereador in lista_presenca:
        if vereador['presente'] == "Presente":
            votacoes = {}
            votos = 0
            for h in lista_votos:
                votacoes[h['votacaoid']] = 1

            for i in lista_votos:
                if i['idparlamentar'] == vereador['idparlamentar'] and i['voto'] != u"Não votou":
                    votos += 1
            #print "Mas votou em apenas " + str(votos) + " matérias de " + str(len(votacoes))
            if votos == 0 and len(votacoes) != 0:
                print vereador['nomeparlamentar'] + ' falou que estava presente mas nunca votou no dia ' + data

votacoes = loadVotacao()
datas = loadSessoes()
for data in datas:
    checaPresenca(data, votacoes)
